unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TfNewGame = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    eNom1: TEdit;
    eNom2: TEdit;
    rgCalibre1: TRadioGroup;
    rgCalibre2: TRadioGroup;
    bOk: TButton;
    rgGrav: TRadioGroup;
    procedure bOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fNewGame: TfNewGame;

implementation

uses Unit1;

{$R *.DFM}


procedure TfNewGame.bOkClick(Sender: TObject);
begin
Joueur1.Nom := eNom1.Text;
Joueur2.Nom := eNom2.Text;

Joueur1.Calibre := rgCalibre1.ItemIndex;
Joueur2.Calibre := rgCalibre2.ItemIndex;

Case rgGrav.ItemIndex of
     0 : Grav := -9.8;
     1 : Grav := -2.3;
   End;
   
fNewGame.Close;
end;

end.
