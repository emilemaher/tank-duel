program TankDuel;

uses
  Forms,
  Unit1 in 'Unit1.pas' {fJeu},
  Unit2 in 'Unit2.pas' {fAbout},
  Unit3 in 'Unit3.pas' {fNewGame},
  Unit4 in 'Unit4.pas' {fInformation};

  {$R *.RES}

begin
  Application.Title := 'Tank Duel 1.0';
  Application.CreateForm(TfJeu, fJeu);
  Application.CreateForm(TfAbout, fAbout);
  Application.CreateForm(TfNewGame, fNewGame);
  Application.CreateForm(TfInformation, fInformation);
  Application.Run;

  end.

